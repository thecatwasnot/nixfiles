# Install
ZFS on root with native encryption.  Systemd-boot.

Create 2 partitions
1. `/boot` formatted vfat for EFI
2. The ZFS dataset partition

Datasets https://grahamc.com/blog/nixos-on-zfs
```
rpool/
├── local
│   └── nix
├── system
│   ├── var
│   └── root
└── user
    ├── root
    └── home
        ├── cole
        └── other
```

# Preperation

Determine the target disks:
```
# For VM 
ls -lh /dev/disk/by-path/*
```

Declare disk:
```
DISK='/dev/disk/by-path/ata-SOMEDISK'
```

# System Installaion
Partition the disks:
```
sgdisk --zap-all ${DISK}
sgdisk -n1:1M:+1G -t1:EF00 ${DISK}
sgdisk -n2:0:0   -t2:BF00 ${DISK}
```
Create root pool:
```
zpool create \
    -o ashift=12 \
    -o autotrim=on \
    -R /mnt \
    -O acltype=posixacl \
    -O canmount=off \
    -O compression=on \
    -O dnodesize=auto \
    -O normalization=formD \
    -O relatime=on \
    -O xattr=sa \
    -O mountpoint=/ \
    -O encryption=on \
    -O keylocation=prompt \
    -O keyformat=passphrase \
    rpool \
    $(printf "${DISK}-part2 ";)
```
Create system container:
```
zfs create \
 -o canmount=off \
 -o mountpoint=none \
 rpool/system
zfs create -o mountpoint=/ rpool/system/root
zfs snapshot rpool/system/root@blank
zfs create -o mountpoint=/var rpool/system/var
```
Create local container:
```
zfs create \
 -o canmount=off \
 -o mountpoint=none \
 rpool/local
zfs create -o mountpoint=/nix rpool/local/nix
```
Create home container:
```
zfs create \
 -o canmount=off \
 -o mountpoint=none \
 rpool/user
zfs create -o mountpoint=/home rpool/user/home
zfs create -o mountpoint=/root rpool/user/root
```

Format and mount ESP:
```
mkfs.vfat -n EFI ${DISK}-part1
mkdir -p /mnt/boot
mount -t vfat ${DISK}-part1 /mnt/boot
```

# System Configuration

Disable cache (stale cache will prevent system from booting):
```
mkdir -p /mnt/etc/zfs/
rm -f /mnt/etc/zfs/zpool.cache
touch /mnt/etc/zfs/zpool.cache
chmod a-w /mnt/etc/zfs/zpool.cache
chattr +i /mnt/etc/zfs/zpool.cache
```

Generate initial system configuration:
```
nixos-generate-config --root /mnt
```

Create nixos configuration for zfs:
```
sed -i "s|./hardware-configuration.nix|./hardware-configuration.nix ./zfs.nix|g" /mnt/etc/nixos/configuration.nix

sed -i '/boot.loader/d' /mnt/etc/nixos/configuration.nix
sed -i '/services.xserver/d' /mnt/etc/nixos/configuration.nix
tee -a /mnt/etc/nixos/zfs.nix <<EOF
{ config, pkgs, ... }:

{ 
  boot.supportedFilesystems = [ "zfs" ];
  networking.hostId = "$(head -c 8 /etc/machine-id)";
  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.enable = true
EOF

# For a VM look for zfs pools by path
tee -a /mnt/etc/nixos/zfs.nix <<EOF
  boot.zfs.devNodes = "/dev/disk/by-path";
EOF

rootPwd=$(mkpasswd -m SHA-512 -s)

tee -a /mnt/etc/nixos/zfs.nix <<EOF
  users.users.root.initialHashedPassword = "${rootPwd}";
}
EOF

# Add zfsutil option
sed -i 's|fsType = "zfs";|fsType = "zfs"; options = [ "zfsutil" "X-mount.mkdir" ];|g' \
/mnt/etc/nixos/hardware-configuration.nix
```

Install system and apply configuration:
```
# Using regular /etc/nix configuration
nixos-install -v --show-trace --no-root-passwd --root /mnt
# Using this flake
nixos-install -v --show-trace --no-root-passwd --root /mnt --flake .#luna
```

Unmount filesystems:
```
umount -Rl /mnt
zpool export -a
```

Reboot:
```
reboot
```

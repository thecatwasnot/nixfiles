{ config, pkgs, ... }:

{ 
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.devNodes = "/dev/disk/by-path";
  boot.zfs.requestEncryptionCredentials = true;

  networking.hostId = "699318da";

  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.enable = true;

  users.users.root.initialHashedPassword = "$6$B7xf4vC2XXf8yXJ5$j.sJdiC35FO2j0BFNkWDzvb4pGqtm4E2x.w0I/B4ucn6f/WWSesuEKLlkcSJ6LL60HtH.hcaEhGJbufmtz8Rk/";
}

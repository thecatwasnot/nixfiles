# Configuration for user cole on all hosts
{ pkgs, ... }: {

  users.mutableUsers = false;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.cole = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    initialPassword = "password";
  };
  programs.zsh.enable = true;
  programs.ssh.startAgent = true;
}

# Configuration for all hosts
{ lib, ... }: {
  imports = [
    ./locale.nix
    ./nix.nix 
  ];
}
